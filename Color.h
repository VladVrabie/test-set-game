#pragma once
class Color
{
public:
	enum class ColorValue
	{
		RED,
		GREEN,
		BLUE
	};

	Color();
	Color(const ColorValue &);
	Color(unsigned);
	virtual ~Color();

	ColorValue getColor() const;

private:
	ColorValue m_color;
};

