#include "Shading.h"



Shading::Shading()
{
}

Shading::Shading(const ShadingValue &shading) : m_shading{ shading }
{
}

Shading::Shading(unsigned shading)
{
	if (shading < 3)
		m_shading = static_cast<ShadingValue>(shading);
	else
		throw "Eroare de initializare";
}

Shading::~Shading()
{
}

Shading::ShadingValue Shading::getShading() const
{
	return m_shading;
}