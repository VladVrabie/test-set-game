#include "Number.h"



Number::Number()
{
}

Number::Number(const NumberValue & value) : m_number{value}
{
}

Number::Number(unsigned number)
{
	if (number < 3)
		m_number = static_cast<NumberValue>(number);
	else
		throw "Eroare de initializare";
}

Number::~Number()
{
}

Number::NumberValue Number::getNumber() const
{
	return m_number;
}