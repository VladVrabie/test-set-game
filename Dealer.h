#pragma once
#include "Card.h"
#include "Deck.h"
#include "GameBoard.h"
#include <string>
class Dealer
{
	std::string m_name;
	GameBoard& m_gameBoard;
	Deck & m_deck;
public:
	Dealer(GameBoard&, Deck&, std::string = "Johnny");
	virtual ~Dealer();

	void first12Card();
	bool put3CardsOnBoard();
};

