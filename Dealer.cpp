#include "Dealer.h"



Dealer::Dealer(GameBoard & gameBoard, Deck &deck, std::string name)
	: m_gameBoard{gameBoard}, m_deck{deck}, m_name{name}
{
}

Dealer::~Dealer()
{
}

void Dealer::first12Card()
{
	for (unsigned i = 0; i < 12; ++i)
		m_gameBoard.putCard(m_deck.getCard());
}

bool Dealer::put3CardsOnBoard()
{
	if (m_deck.getDeckSize() && m_deck.getDeckSize() % 3 == 0)
	{
		for (unsigned i = 0; i < 3; ++i)
			m_gameBoard.putCard(m_deck.getCard());
		return true;
	}
	return false;
}