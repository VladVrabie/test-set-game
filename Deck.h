#pragma once
#include "Card.h"
#include <vector>
#include <string>
class Deck
{
	std::vector<Card> m_deck;
	std::string m_name;
public:
	Deck(std::string = "Francois");
	virtual ~Deck();

	void shuffle();
	Card getCard();

	size_t getDeckSize() const;
};

