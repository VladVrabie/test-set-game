#include "Deck.h"
#include <random>
#include <algorithm>


Deck::Deck(std::string name) : m_name{name}
{
	for (unsigned color = 0; color < 3; ++color)
		for (unsigned number = 0; number < 3; ++number)
			for (unsigned shading = 0; shading < 3; ++shading)
				for (unsigned symbol = 0; symbol < 3; ++symbol)
					m_deck.emplace_back(color, number, shading, symbol);
	shuffle();

}

Deck::~Deck()
{
	m_deck.clear();
}

void Deck::shuffle()
{
	std::random_device rd;
	std::mt19937 generator(rd());

	std::shuffle(m_deck.begin(), m_deck.end(), generator);
}

Card Deck::getCard()
{
	Card card = m_deck.back();
	m_deck.pop_back();
	return card;
}

size_t Deck::getDeckSize() const
{
	return m_deck.size();
}