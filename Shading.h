#pragma once
class Shading
{
public:
	enum class ShadingValue
	{
		SOLID,
		STRIPED,
		OPEN
	};

	Shading();
	Shading(const ShadingValue &);
	Shading(unsigned);
	virtual ~Shading();

	ShadingValue getShading() const;

private:
	ShadingValue m_shading;
};

