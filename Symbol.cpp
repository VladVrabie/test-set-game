#include "Symbol.h"



Symbol::Symbol()
{
}

Symbol::Symbol(const SymbolValue &symbol) : m_symbol{symbol}
{
}

Symbol::Symbol(unsigned symbol)
{
	if (symbol < 3)
		m_symbol = static_cast<SymbolValue>(symbol);
	else
		throw "Eroare de initializare";
}

Symbol::~Symbol()
{
}

Symbol::SymbolValue Symbol::getSymbol() const
{
	return m_symbol;
}