#pragma once
#include "Dealer.h"
#include "GameBoard.h"
#include "Deck.h"
#include <string>
//#include ""
class Game
{
	GameBoard m_gameBoard;
	Deck m_deck;
	Dealer m_dealer;
public:
	Game(std::string, std::string, std::string);
	virtual ~Game();

	bool F1();
	bool F2(unsigned, unsigned, unsigned);
};

