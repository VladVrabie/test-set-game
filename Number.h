#pragma once
class Number
{
public:
	enum class NumberValue
	{
		ONE,
		TWO,
		THREE
	};

	Number();
	Number(const NumberValue &);
	Number(unsigned);
	virtual ~Number();

	NumberValue getNumber() const;

private:
	NumberValue m_number;
};

