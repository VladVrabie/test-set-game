#include "GameBoard.h"



GameBoard::GameBoard(std::string name)
{
}


GameBoard::~GameBoard()
{
}

void GameBoard::putCard(const Card & card)
{
	m_board.push_back(card);
}

bool GameBoard::checkBoardSize() const
{
	return m_board.size() > 11;
}

size_t GameBoard::getBoardSize() const
{
	return m_board.size();
}

bool GameBoard::checkSet(unsigned first, unsigned second, unsigned third)
{

	return checkColor(first, second, third)
		&& checkNumber(first, second, third)
		&& checkShading(first, second, third)
		&& checkSymbol(first, second, third);
}
bool GameBoard::checkNumber(unsigned first, unsigned second, unsigned third)
{
	return (m_board[first].getNumber() == m_board[second].getNumber()
		&& m_board[first].getNumber() == m_board[third].getNumber() )
		|| 
		(m_board[first].getNumber() != m_board[second].getNumber()
		 && m_board[first].getNumber() != m_board[third].getNumber()
		 && m_board[third].getNumber() != m_board[second].getNumber() );
}

bool GameBoard::checkSymbol(unsigned first, unsigned second, unsigned third)
{
	return (m_board[first].getSymbol() == m_board[second].getSymbol()
		&& m_board[first].getSymbol() == m_board[third].getSymbol())
		||
		(m_board[first].getSymbol() != m_board[second].getSymbol()
			&& m_board[first].getSymbol() != m_board[third].getSymbol()
			&& m_board[third].getSymbol() != m_board[second].getSymbol());
}

bool GameBoard::checkColor(unsigned first, unsigned second, unsigned third)
{
	return (m_board[first].getColor() == m_board[second].getColor()
		&& m_board[first].getColor() == m_board[third].getColor())
		||
		(m_board[first].getColor() != m_board[second].getColor()
			&& m_board[first].getColor() != m_board[third].getColor()
			&& m_board[third].getColor() != m_board[second].getColor());
}

bool GameBoard::checkShading(unsigned first, unsigned second, unsigned third)
{
	return (m_board[first].getShading() == m_board[second].getShading()
		&& m_board[first].getShading() == m_board[third].getShading())
		||
		(m_board[first].getShading() != m_board[second].getShading()
			&& m_board[first].getShading() != m_board[third].getShading()
			&& m_board[third].getShading() != m_board[second].getShading());
}

Card GameBoard::getCard(unsigned index) const
{
	return m_board.at(index);
}

void GameBoard::removeCard( Card & card)
{
	for (auto i = m_board.begin(); i != m_board.end(); ++i )
	{
		if (*i == card)
		{
			m_board.erase(i);
			return;
		}
	}
}