#pragma once
#include "Color.h"
#include "Number.h"
#include "Shading.h"
#include "Symbol.h"

class Card : public Color, public Number, public Shading, public Symbol
//nu e bine cu mostenire, dar fara nu am acces la getteri
{
public:
	Card();
	Card(unsigned, unsigned, unsigned, unsigned);
	virtual ~Card();

	void printCard() const;
private:
	Color m_cardColor;
	Number m_cardNumber;
	Shading m_cardShading;
	Symbol m_cardSymbol;
};

