#include "Color.h"



Color::Color()
{
}

Color::Color(const ColorValue &color) : m_color{ color }
{
}

Color::Color(unsigned color)
{
	if (color < 3)
		m_color = static_cast<ColorValue>(color);
	else
		throw "Eroare de initializare";

}

Color::~Color()
{
}

Color::ColorValue Color::getColor() const
{
	return m_color;
}