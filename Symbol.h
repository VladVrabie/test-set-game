#pragma once
class Symbol
{
public:
	enum class SymbolValue
	{
		DIAMOND,
		SQUIGGLE,
		OVAL
	};

	Symbol();
	Symbol(const SymbolValue &);
	Symbol(unsigned);
	virtual ~Symbol();

	SymbolValue getSymbol() const;

private:
	SymbolValue m_symbol;
};

