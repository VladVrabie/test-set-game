#pragma once
#include "Card.h"
#include <string>
#include <vector>
class GameBoard
{
	std::vector<Card> m_board;
	std::string m_name;
public:
	GameBoard(std::string = "Gerhart");
	virtual ~GameBoard();

	void putCard(const Card &);
	void removeCard( Card &);

	bool checkBoardSize() const;
	size_t getBoardSize() const;

	Card getCard(unsigned) const;

	bool checkSet(unsigned, unsigned, unsigned);
	bool checkNumber(unsigned, unsigned, unsigned);
	bool checkSymbol(unsigned, unsigned, unsigned);
	bool checkColor(unsigned, unsigned, unsigned);
	bool checkShading(unsigned, unsigned, unsigned);

};

