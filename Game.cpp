#include "Game.h"



Game::Game(std::string boardName, std::string deckName, std::string dealerName)
	: m_gameBoard(boardName), m_deck(deckName), m_dealer(m_gameBoard, m_deck, dealerName)
{
	m_dealer.first12Card();
}


Game::~Game()
{
}

bool Game::F2(unsigned first, unsigned second, unsigned third)
{
	if (m_gameBoard.checkSet(first, second, third))
	{
		Card firstCard = m_gameBoard.getCard(first);
		Card secondCard = m_gameBoard.getCard(second);
		Card thirdCard = m_gameBoard.getCard(third);
		m_gameBoard.removeCard(firstCard);
		m_gameBoard.removeCard(secondCard);
		m_gameBoard.removeCard(thirdCard);
		return true;
	}
	return false;
}